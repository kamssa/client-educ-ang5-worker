import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AcceuilComponent} from './shared/cadre/acceuil/acceuil.component';


const routes: Routes = [

  {path: '', redirectTo: '/accueil', pathMatch: 'full'},
  {path: 'accueil', component: AcceuilComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
