import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.scss']
})
export class AcceuilComponent implements OnInit {
  title = 'GESTION ECOLE POUR UNE FORMATION DE QUALITE ET D EXCELLENCE';
  constructor() { }

  ngOnInit() {
  }

}
